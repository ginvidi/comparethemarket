var cardLister = (function () {

  // Define the variables used by module
  var init,
      callJson,
      seeCardDetails,
      jsonToHTML,
      addHTMLToDom,
      showMenuOnMobile,
      cardListId = document.getElementById("cards-list"),
      hamburgerMenuIcon = document.getElementById("hamburger");


  // The "init" function, sets up event handlers
  init = function (){
    callJson();
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].onclick = function(){
      }
    }
    //show card details by clicking on panel header
    cardListId.addEventListener("click", function (event){
      seeCardDetails(event.target.getAttribute("data-code"));
    });
    //show menu on mobile
    hamburgerMenuIcon.addEventListener("click", function (event){
      showMenuOnMobile();
    });
  };

  // Ajax call Json
  callJson = function (){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'data/cards.json');
    xhr.send(null);
    xhr.onreadystatechange = function (){
      var DONE = 4; // readyState 4 means the request is done.
      var OK = 200; // status 200 is a successful return.
      if (xhr.readyState === DONE) {
        if (xhr.status === OK){
          console.log("It worked"); // 'This is the returned text.'
          jsonToHTML(JSON.parse(xhr.response));
        }
        else {
          console.log('Error: ' + xhr.status); // An error occurred during the request.
        }
      }
    };
  };

  //show menu on device undert 1024px
  showMenuOnMobile = function (card){
    var menuList = document.getElementById("menu");
    hamburgerMenuIcon.classList.toggle("opened");
    menuList.classList.toggle("show");
  };

  //Display panel details
  seeCardDetails = function (card){
    var cardId = document.getElementById(card);
    cardId.classList.toggle("show");
    cardId.previousElementSibling.getElementsByTagName("h3")[0].getElementsByTagName("img")[0].classList.toggle("show");
  };

  // Function to convert data returned via ajax to HTML
  jsonToHTML = function (cardsList) {
    var cardView = [],
        cardViewStr = "";
    //loop Json and create for each element of the list the follow HTML component
    for (card in cardsList){
      cardView.push(
        '<div class="card" data-code="' + cardsList[card]["code"] + '">',
          '<div>',
            '<div class="panel panel-primary">',
              '<div class="panel-heading">',
                '<h3 class="panel-title" data-code="' + cardsList[card]["code"] + '">',
                 '<img src="assets/arrow.png" alt="arrow" class="arrow">',
                  '<p  data-code="' + cardsList[card]["code"] + '">',
                    cardsList[card]["name"],
                  '</p>',
                  '<p class="pull-right"  data-code="' + cardsList[card]["code"] + '">',
                  cardsList[card]["apr"] + "% APR",
                  '</stropng>',
                '</h3>',
              '</div>',
              '<div class="panel-body card-info" id="' + cardsList[card]["code"] + '">',
                '<div class="card-img">',
                  '<img class="pull-right" src="assets/' + cardsList[card]["code"].toLowerCase() + '.png" alt="' + cardsList[card]["name"] + '">',
                '</div>',
                '<div class="information">',
                  '<p>',
                    cardsList[card]["information"],
                  '</p>',
                '</div>',
                '<div class="cashback">',
                  '<h4>',
                    'CashBack',
                  '</h4>',
                  '<h3>',
                    "&#163;" + cardsList[card]["cashback"].toFixed(2),
                  '</h3>',
                '</div>',
              '</div>',
            '</div>',
          '</div>',
        '</div>'
        )
    }

    cardViewStr = cardView.join("");
    addHTMLtoDOM(cardViewStr);
  };

  // Function to add the HTML to the DOM
  addHTMLtoDOM = function (html) {
    cardListId.insertAdjacentHTML('beforeend', html);
  };

  // All we need to return is the init function
  return {
    init: init
  };

})();

// Initialise "cardLister"
cardLister.init();
